'use strict';

module.exports = function (grunt) {

  // initial config
  grunt.initConfig({
    
    pkg: grunt.file.readJSON('package.json'),
        
    ngAnnotate: {
      options: {
        singleQuotes: true,
      },
      app: {
        files: {
          // app
          './temp/app-annotate.js': [
            './dist/app/*.js',
            './dist/app/config/*.js',
            './dist/app/services/**/*.js',
            './dist/app/directives/**/*.js',
            './dist/app/layouts/**/*.js'
          ]
        }
      }
    },
    concat: {
  	  options: {
        separator: ';',
      },
      js_frontend: {
        src: [          
          './vendor/jquery/dist/jquery.js',
          './vendor/bootstrap/dist/js/bootstrap.js',
          './vendor/api-check/dist/api-check.js',          
          './vendor/angular/angular.js',
          './vendor/angular-ui-router/release/angular-ui-router.js',
          './vendor/angular-animate/angular-animate.js',
          './vendor/satellizer/satellizer.js',
          './vendor/angular-smart-table/dist/smart-table.js',
          './vendor/moment/moment.js',
          './vendor/underscore/underscore.js',
          './vendor/angular-formly/dist/formly.js',
          './vendor/angular-formly-templates-bootstrap/dist/angular-formly-templates-bootstrap.js',          
          './vendor/angular-translate/angular-translate.js'
        ],
        dest: './dist/js/admin-ap.js'
      },
      js_app: {
        src: [          
          './temp/app-annotate.js',
        ],
        dest: './dist/js/app.js'
      }
  	},
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> - <%= pkg.version %> js created at <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          './dist/js/admin-ap.min.js': './dist/js/admin-ap.js',
          './dist/js/app.min.js': './dist/js/app.js'
        }
      }
    },
    copy: {
      css: {
        files: [
          // bootstrap fonts
          {expand: true, cwd: './vendor/bootstrap/dist/fonts/', src: '**/*', dest: './dist/fonts/'},
          // fontawesome fonts
          {expand: true, cwd: './vendor/fontawesome/fonts/', src: '**/*', dest: './dist/fonts/'},
          // flags icons
          {expand: true, cwd: './vendor/flag-icon-css/flags/', src: '**/*', dest: './dist/flags/'},
        ]
      },
      dev: {
        files: [
          // services url
          {expand: true, dot:true, cwd: './dist/app/config/', src: '*.dev', dest: './dist/app/config/', rename: function(dest, src) {return dest + src.replace('.dev','');}},          
          // mock data
          {expand: true, cwd: './mock/', src: '*.json', dest: './dist/mock/'},
        ]
      },
      dist: {
        files: [
          // services url
          {expand: true, dot:true, cwd: './dist/app/config/', src: '*.dist', dest: './dist/app/config/', rename: function(dest, src) {return dest + src.replace('.dist','');}},
        ]
      },
    },
    less: {
      main: {
        files: {
          './temp/custom.css': './custom/custom.less',
          './temp/app.css': './dist/**/*.less'
        }
      }
    },
    cssmin: {
      dist: {
        options: {
           banner: '/*! <%= pkg.name %> - <%= pkg.version %> css created at <%= grunt.template.today("dd-mm-yyyy") %> */\n'
        },
        files: {
           './dist/css/admin-ap.min.css': ['./vendor/bootstrap/dist/css/bootstrap.css','./vendor/fontawesome/css/font-awesome.css','./vendor/flag-icon-css/css/flag-icon.css','./temp/custom.css','./temp/app.css']
        }
      }
    },
    processhtml: {
      options: {
        data: {
          suffix: '<%= grunt.template.today("yyyymmdd") %>',
          title: 'App title'
        }
      },
      dev: {
        options: {
          process: true
        },
        files: {
          './dist/index.html': ['./templates/index.html']
        }
      },
      dist: {
        files: {
          './dist/index.html': ['./templates/index.html']
        }
      }
    },
    watch: {
      less: {
        files: ['./**/*.less'],
        tasks: ['less','cssmin']
      },
      template: {
        files: ['./templates/*.*'],
        tasks: ['processhtml:dev']
      },
      app: {
        files: ['./dist/app/**/*.*'],
        tasks: ['copy:dev','ngAnnotate','concat','clean:temp']
      },
      mock: {
        files: ['./mock/*.*'],
        tasks: ['copy:dev']
      }
    },
    clean: {
      dist: ['./dist/js','./dist/css','./dist/fonts','./dist/mock'],
      temp: ['./temp']      
    },
    connect: {
      options: {
        port: 9000
      },
      dev: {
        options: {
          open: true,
          middleware: function (connect) {
            return [
              connect().use( '/', connect.static('./dist')),
            ];
          }
        }
      }
    }    
    
  });
  
  // initialize plugins
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');  
  grunt.loadNpmTasks('grunt-ng-annotate');  
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-processhtml');
  
  // register tasks
  grunt.registerTask('dev', [    
    'clean:dist',
    'copy:dev',
    'ngAnnotate',
    'concat',
    'copy:css',
    'less',
    'cssmin',
    'clean:temp',
    'processhtml:dev'    
  ]);    
  
  grunt.registerTask('dist', [    
    'clean:dist',
    'copy:dist',
    'ngAnnotate',
    'concat',
    'uglify',
    'copy:css',
    'less',
    'cssmin',
    'clean:temp',
    'processhtml:dist'
  ]);
  
  grunt.registerTask('serve', [
    'dev',
    'connect:dev',
    'watch'
  ]);
  
  grunt.registerTask('default', ['serve']);
  
};
