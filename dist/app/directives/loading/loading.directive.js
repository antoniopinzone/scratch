(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('loadingComponent', loadingComponent);

	function loadingComponent() {
		var directive = {
        restrict: 'E',
				templateUrl: 'app/directives/loading/loading.html',
		};
		return directive;
	};
  
})();
