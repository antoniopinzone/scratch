(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('breadcrumbComponent', breadcrumbComponent);

	function breadcrumbComponent() {
		var directive = {
        restrict: 'E',
        scope: {
					list: '='
				},
				templateUrl: 'app/directives/breadcrumb/breadcrumb.html',
        controller: BreadcrumbController,
        controllerAs: 'vm',
        bindToController: true
		};
		return directive;
	};
  
  function BreadcrumbController() {
    var vm = this;
  };
  
})();
