(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('loginComponent', loginComponent);

	function loginComponent() {
		var directive = {
        restrict: 'E',
				scope: {
					onSubmit: '=',
          loading: '=?'
				},
				templateUrl: 'app/directives/login/login.html',
        controller: LoginController,
        controllerAs: 'vm',
        bindToController: true				
		};
		return directive;
	};
  
  function LoginController() {
    var vm = this;

    vm.login = {
      user: '',
      password: ''
    };
    
    if (_.isUndefined(vm.loading))
      vm.loading = false;
  };
  
})();
