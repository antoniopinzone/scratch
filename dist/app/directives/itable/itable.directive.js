(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('itableComponent', itableComponent);

	function itableComponent() {
		var directive = {
        restrict: 'E',
				scope: {
					table: '='
				},
				templateUrl: 'app/directives/itable/itable.html',
        controller: TableController,
        controllerAs: 'vm',
        bindToController: true				
		};
		return directive;
	};
  
  function TableController() {
    var vm = this;
    
    vm.orderBy = function(column) {
      if (!column.orderable)
        return null;
      
      if (_.isUndefined(column.order))
        column.order = 'desc';
        
      if (column.order=='asc') {
        vm.table.data = _.sortBy(vm.table.data,column.id).reverse();
        column.order = 'desc';
      } else {
        vm.table.data = _.sortBy(vm.table.data,column.id)
        column.order = 'asc';
      }
      
      vm.table.orderedBy = _.indexOf(vm.table.columns,column);
    };
    
    if (!_.isUndefined(vm.table.orderBy)) 
      vm.orderBy(vm.table.columns[vm.table.orderBy]);
      
  };
  
})();
