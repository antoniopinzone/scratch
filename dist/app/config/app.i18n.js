(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .config(function ($translateProvider) {
  
        $translateProvider.translations('en', {
            LAYOUT: {
              FOOTER: 'Place sticky footer content here.',
              LOGOUT: 'Logout',
              SIGNED: 'Signed in as',
              LANGUAGE: 'Italiano'
            },
            LOGIN: {
              LOGIN: 'Login',
              GO: 'Login',
              USER: 'E-Mail',
              PASSWORD: 'Password'
            },
            LIST: {
              SEARCH: 'Search',
              DATA: {
                ID: 'Id',
                NAME: 'Name',
                EMAIL: 'E-mail',
              }
            },
            DETAIL: {
              BREADCRUMB: {
                PREV: 'Item list',
                ME: 'Update item'
              },
              FORM: {
                NAME: 'Name',
                EMAIL: 'E-Mail',
                ADDRESS: 'Address',
                STATE: 'State',
              },
              SUBMIT: 'Update'
            }
        });
  
        $translateProvider.translations('it', {
            LAYOUT: {
              FOOTER: 'Scrivi il contenuto del tuo footer qui.',
              LOGOUT: 'Scollegati',
              SIGNED: 'Collegato come',
              LANGUAGE: 'English'
            },
            LOGIN: {
              LOGIN: 'Autenticazione',
              GO: 'Autenticati',
              USER: 'E-Mail',
              PASSWORD: 'Password'
            },
            LIST: {
              SEARCH: 'Cerca',
              DATA: {
                ID: 'Id',
                NAME: 'Nome',
                EMAIL: 'E-mail',
              }
            },
            DETAIL: {
              BREADCRUMB: {
                PREV: 'Elenco item',
                ME: 'Aggiorna item'
              },
              FORM: {
                NAME: 'Nome',
                EMAIL: 'E-Mail',
                ADDRESS: 'Indirizzo',
                STATE: 'Stato',
              },
              SUBMIT: 'Aggiorna'
            }
        });
  
        $translateProvider.preferredLanguage('it');
        $translateProvider.useSanitizeValueStrategy(null);
  
    });

})();