(function () {
	'use strict';

	angular
    .module('admin-ap')
    .config(routeConfig);	
	
	// routing configuration
	routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routeConfig($stateProvider, $urlRouterProvider) {
    
    // redirect to the auth state 
    $urlRouterProvider.otherwise('/list');

		// all others states
		$stateProvider
  		.state('login', {
  			url: '/login',
  			templateUrl: 'app/layouts/login/login.html',
  			controller: 'LoginController',
  			controllerAs: 'vm'
  		})
      .state('list', {
  			url: '/list',
  			templateUrl: 'app/layouts/list/list.html',
  			controller: 'ListController',
  			controllerAs: 'vm',
        resolve: {
          loginRequired: loginRequired
        }
  		})
      .state('detail', {
  			url: '/detail/:itemId',
  			templateUrl: 'app/layouts/detail/detail.html',
  			controller: 'DetailController',
  			controllerAs: 'vm',
        resolve: {
          loginRequired: loginRequired
        }
  		});
      
    function loginRequired($q, $location, authenticationService) {
      var deferred = $q.defer();
      if (authenticationService.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/login');
      }
      return deferred.promise;
    }
	
  };

})();