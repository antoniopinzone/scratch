(function () {
	'use strict';

	angular
  	.module('admin-ap')
  	.constant('CONSTANTS',{
      'AUTH_SERVICE': '/mock/auth.json',
      'USERS_SERVICE': '/mock/users.json',
      'USER_SERVICE': '/mock/user.json'
  	});

})();