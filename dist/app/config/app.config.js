(function () {
	'use strict';

	angular
    .module('admin-ap')
    .run(runConfig)
    .config(authConfig)
    .config(logConfig);
	
	// auth configuration
	runConfig.$inject = ['$rootScope', 'authenticationService', 'translationService'];
	function runConfig($rootScope, authenticationService, translationService) {
		// global services function
    $rootScope.authentication = authenticationService;
    $rootScope.translation = translationService;
	};
  
  // auth configuration
	authConfig.$inject = ['$authProvider', 'CONSTANTS'];
	function authConfig($authProvider, CONSTANTS) {
		// satellizer configuration
    $authProvider.loginUrl = CONSTANTS.AUTH_SERVICE;
    $authProvider.tokenName = 'token';
	};
  
  // log configuration
	logConfig.$inject = ['$logProvider'];
	function logConfig($logProvider) {
		// log level --> DEBUG
		$logProvider.debugEnabled(true);
	};

})();