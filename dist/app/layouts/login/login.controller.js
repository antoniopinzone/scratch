(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .controller('LoginController', LoginController);
  
  function LoginController($log, $state, authenticationService) {
      $log.info("LoginController loaded.");
      
      var vm = this;
      
      vm.loading = false;
      
      vm.onSubmit = function(credentials) {
        vm.loading = true;
        // call auth service
        authenticationService.login(credentials.user, credentials.password)
          .then(function (data) {
            vm.loading = false;
            $state.go('list');
          })
          .catch(function (error) {
            vm.loading = true;
            vm.error = true;
            $log.error("AUTH ERROR > ");
            $log.error(error);
          });
      };
  };
    
})();
