(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .controller('DetailController', DetailController);
  
  function DetailController($log, $stateParams, $state, $http, $translate, $scope, CONSTANTS) {
      $log.info("DetailController loaded.");
      
      var vm = this;
      
      vm.loading = true;
      
      vm.breadcrumb = [
        {active:false, label:'DETAIL.BREADCRUMB.PREV', link:'list'},
        {active:true, label:'DETAIL.BREADCRUMB.ME'}
      ];
      
      $http({
        method : "GET",
        url : CONSTANTS.USER_SERVICE
      }).then(function success(response) {        
        
        // The model object that we reference
        // on the  element in detail.html
        vm.item = {};
        
        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        vm.itemFields = [
            {
              key: 'name',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.NAME'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.NAME" | translate'
              }
            },
            {
              key: 'address',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.ADDRESS'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.ADDRESS" | translate'
              }
            },
            {
              key: 'state',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.STATE'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.STATE" | translate'
              }
            },
            {
              key: 'email',
              type: 'input',
              templateOptions: {
                type: 'email',
                label: $translate.instant('DETAIL.FORM.EMAIL'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.EMAIL" | translate'
              }
            },
        ];
        
        vm.item = response.data.data;        
        vm.loading = false;        
        
      }, function error(response) {
        vm.loading = false;
        $log.error(response.statusText);
      });
      
      vm.onSubmit = function() {
        console.log(vm.item);
      };
      
      $scope.$watch(function() {
        return $translate.use();
      }, function(newLang, oldLang) {
        if (newLang && oldLang && newLang !== oldLang) {
          angular.forEach(vm.itemFields, function(field) {
            field.runExpressions && field.runExpressions();
          });
        }
      });   
  };
    
})();
