(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .controller('ListController', ListController);
  
  function ListController($log, $http, CONSTANTS) {
      $log.info("ListController loaded.");
      
      var vm = this;
      
      vm.loading = true;
      
      $http({
        method : "GET",
        url : CONSTANTS.USERS_SERVICE
      }).then(function success(response) {
        vm.table = {
          columns: [
            {id:'index', label:'LIST.DATA.ID', orderable:true},
            {id:'name', label:'LIST.DATA.NAME', orderable:false},
            {id:'email', label:'LIST.DATA.EMAIL', orderable:true}
          ],
          orderBy: 0, // optional
          id: 'index', // id field for details
          data: response.data.data
        };
        vm.loading = false;        
      }, function error(response) {
        vm.loading = false;
        $log.error(response.statusText);
      });
      
      
  };
    
})();
