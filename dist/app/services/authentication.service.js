(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('authenticationService', authenticationService);
  
  authenticationService.$inject = ['$location','$auth','utilsService'];
  
  function authenticationService($location, $auth, utilsService) {
    return {
        isAuthenticated: function () {
            return $auth.isAuthenticated();
        },
        isSuperUser: function () {
            if ($auth.isAuthenticated() && $auth.getPayload().role == "Admin")
                return true;
            return false;
        },
        getRole: function () {
            if ($auth.isAuthenticated())
                return $auth.getPayload().role;
            return '';
        },
        getUser: function () {
            if ($auth.isAuthenticated())
                return $auth.getPayload().sub;
            return '';
        },
        logout: function () {
            $auth.logout();
            $location.path('/login');
        },
        login: function login(user, password) {
            var loginOptions = {
                paramSerializer: '$httpParamSerializerJQLike',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                method: 'GET'
            };
            return $auth.login("grant_type=password&username=" + escape(user) + "&password=" + escape(password) + "&resource=" + utilsService.getDomain(), loginOptions);
        }
    };
  };

})();