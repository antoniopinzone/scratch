(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('translationService', translationService);
  
  translationService.$inject = ['$translate'];
  
  function translationService($translate) {
      return {
        setLanguage: function (lang) {
            $translate.use(lang);
        },
        getLabel: function getLabel(label) {
            return $translate.instant(label);
        }
      };
  };

})();