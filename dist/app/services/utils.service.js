(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('utilsService', utilsService);
  
  utilsService.$inject = ['$location'];
  
  function utilsService($location) {
    return {
        getDomain: function () {
            return $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/";
        },
        randomPassword: function () {
            var letters = "abcdefghijklmnopqrstuvwxyz";
            var chars = "!@#$";
            var numbers = "1234567890";
  
            var pass = "";
  
            // 4 letters lower
            for (var i = 0; i < 4; i++)
                pass += letters.charAt(Math.floor(Math.random() * letters.length));
  
            // 1 number
            for (var i = 0; i < 1; i++)
                pass += numbers.charAt(Math.floor(Math.random() * numbers.length));
  
            // 1 char
            for (var i = 0; i < 1; i++)
                pass += chars.charAt(Math.floor(Math.random() * chars.length));
  
            // 4 letters upper
            for (var i = 0; i < 4; i++)
                pass += letters.charAt(Math.floor(Math.random() * letters.length)).toUpperCase();
  
            return pass;
        }
    };
  };

})();