(function () {
  'use strict';

  angular
    .module('admin-ap', [
      'ui.router',
      'ngAnimate',
      'satellizer',
      'smart-table',
      'formly',
      'formlyBootstrap',      
      'pascalprecht.translate'
    ]);
       
})();
