(function () {
  'use strict';

  angular
    .module('admin-ap', [
      'ui.router',
      'ngAnimate',
      'satellizer',
      'smart-table',
      'formly',
      'formlyBootstrap',      
      'pascalprecht.translate'
    ]);
       
})();

(function () {
	'use strict';

	angular
    .module('admin-ap')
    .run(runConfig)
    .config(authConfig)
    .config(logConfig);
	
	// auth configuration
	runConfig.$inject = ['$rootScope', 'authenticationService', 'translationService'];
	function runConfig($rootScope, authenticationService, translationService) {
		// global services function
    $rootScope.authentication = authenticationService;
    $rootScope.translation = translationService;
	};
  
  // auth configuration
	authConfig.$inject = ['$authProvider', 'CONSTANTS'];
	function authConfig($authProvider, CONSTANTS) {
		// satellizer configuration
    $authProvider.loginUrl = CONSTANTS.AUTH_SERVICE;
    $authProvider.tokenName = 'token';
	};
  
  // log configuration
	logConfig.$inject = ['$logProvider'];
	function logConfig($logProvider) {
		// log level --> DEBUG
		$logProvider.debugEnabled(true);
	};

})();
(function () {
	'use strict';

	angular
  	.module('admin-ap')
  	.constant('CONSTANTS',{
      'AUTH_SERVICE': '/mock/auth.json',
      'USERS_SERVICE': '/mock/users.json',
      'USER_SERVICE': '/mock/user.json'
  	});

})();
(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .config(['$translateProvider', function ($translateProvider) {
  
        $translateProvider.translations('en', {
            LAYOUT: {
              FOOTER: 'Place sticky footer content here.',
              LOGOUT: 'Logout',
              SIGNED: 'Signed in as',
              LANGUAGE: 'Italiano'
            },
            LOGIN: {
              LOGIN: 'Login',
              GO: 'Login',
              USER: 'E-Mail',
              PASSWORD: 'Password'
            },
            LIST: {
              SEARCH: 'Search',
              DATA: {
                ID: 'Id',
                NAME: 'Name',
                EMAIL: 'E-mail',
              }
            },
            DETAIL: {
              BREADCRUMB: {
                PREV: 'Item list',
                ME: 'Update item'
              },
              FORM: {
                NAME: 'Name',
                EMAIL: 'E-Mail',
                ADDRESS: 'Address',
                STATE: 'State',
              },
              SUBMIT: 'Update'
            }
        });
  
        $translateProvider.translations('it', {
            LAYOUT: {
              FOOTER: 'Scrivi il contenuto del tuo footer qui.',
              LOGOUT: 'Scollegati',
              SIGNED: 'Collegato come',
              LANGUAGE: 'English'
            },
            LOGIN: {
              LOGIN: 'Autenticazione',
              GO: 'Autenticati',
              USER: 'E-Mail',
              PASSWORD: 'Password'
            },
            LIST: {
              SEARCH: 'Cerca',
              DATA: {
                ID: 'Id',
                NAME: 'Nome',
                EMAIL: 'E-mail',
              }
            },
            DETAIL: {
              BREADCRUMB: {
                PREV: 'Elenco item',
                ME: 'Aggiorna item'
              },
              FORM: {
                NAME: 'Nome',
                EMAIL: 'E-Mail',
                ADDRESS: 'Indirizzo',
                STATE: 'Stato',
              },
              SUBMIT: 'Aggiorna'
            }
        });
  
        $translateProvider.preferredLanguage('it');
        $translateProvider.useSanitizeValueStrategy(null);
  
    }]);

})();
(function () {
	'use strict';

	angular
    .module('admin-ap')
    .config(routeConfig);	
	
	// routing configuration
	routeConfig.$inject = ['$stateProvider', '$urlRouterProvider'];
	function routeConfig($stateProvider, $urlRouterProvider) {
    
    // redirect to the auth state 
    loginRequired.$inject = ['$q', '$location', 'authenticationService'];
    $urlRouterProvider.otherwise('/list');

		// all others states
		$stateProvider
  		.state('login', {
  			url: '/login',
  			templateUrl: 'app/layouts/login/login.html',
  			controller: 'LoginController',
  			controllerAs: 'vm'
  		})
      .state('list', {
  			url: '/list',
  			templateUrl: 'app/layouts/list/list.html',
  			controller: 'ListController',
  			controllerAs: 'vm',
        resolve: {
          loginRequired: loginRequired
        }
  		})
      .state('detail', {
  			url: '/detail/:itemId',
  			templateUrl: 'app/layouts/detail/detail.html',
  			controller: 'DetailController',
  			controllerAs: 'vm',
        resolve: {
          loginRequired: loginRequired
        }
  		});
      
    function loginRequired($q, $location, authenticationService) {
      var deferred = $q.defer();
      if (authenticationService.isAuthenticated()) {
        deferred.resolve();
      } else {
        $location.path('/login');
      }
      return deferred.promise;
    }
	
  };

})();
(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('authenticationService', authenticationService);
  
  authenticationService.$inject = ['$location','$auth','utilsService'];
  
  function authenticationService($location, $auth, utilsService) {
    return {
        isAuthenticated: function () {
            return $auth.isAuthenticated();
        },
        isSuperUser: function () {
            if ($auth.isAuthenticated() && $auth.getPayload().role == "Admin")
                return true;
            return false;
        },
        getRole: function () {
            if ($auth.isAuthenticated())
                return $auth.getPayload().role;
            return '';
        },
        getUser: function () {
            if ($auth.isAuthenticated())
                return $auth.getPayload().sub;
            return '';
        },
        logout: function () {
            $auth.logout();
            $location.path('/login');
        },
        login: function login(user, password) {
            var loginOptions = {
                paramSerializer: '$httpParamSerializerJQLike',
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
                method: 'GET'
            };
            return $auth.login("grant_type=password&username=" + escape(user) + "&password=" + escape(password) + "&resource=" + utilsService.getDomain(), loginOptions);
        }
    };
  };

})();
(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('translationService', translationService);
  
  translationService.$inject = ['$translate'];
  
  function translationService($translate) {
      return {
        setLanguage: function (lang) {
            $translate.use(lang);
        },
        getLabel: function getLabel(label) {
            return $translate.instant(label);
        }
      };
  };

})();
(function () {
  'use strict';
  
  angular
    .module('admin-ap')
    .factory('utilsService', utilsService);
  
  utilsService.$inject = ['$location'];
  
  function utilsService($location) {
    return {
        getDomain: function () {
            return $location.protocol() + "://" + $location.host() + ":" + $location.port() + "/";
        },
        randomPassword: function () {
            var letters = "abcdefghijklmnopqrstuvwxyz";
            var chars = "!@#$";
            var numbers = "1234567890";
  
            var pass = "";
  
            // 4 letters lower
            for (var i = 0; i < 4; i++)
                pass += letters.charAt(Math.floor(Math.random() * letters.length));
  
            // 1 number
            for (var i = 0; i < 1; i++)
                pass += numbers.charAt(Math.floor(Math.random() * numbers.length));
  
            // 1 char
            for (var i = 0; i < 1; i++)
                pass += chars.charAt(Math.floor(Math.random() * chars.length));
  
            // 4 letters upper
            for (var i = 0; i < 4; i++)
                pass += letters.charAt(Math.floor(Math.random() * letters.length)).toUpperCase();
  
            return pass;
        }
    };
  };

})();
(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('breadcrumbComponent', breadcrumbComponent);

	function breadcrumbComponent() {
		var directive = {
        restrict: 'E',
        scope: {
					list: '='
				},
				templateUrl: 'app/directives/breadcrumb/breadcrumb.html',
        controller: BreadcrumbController,
        controllerAs: 'vm',
        bindToController: true
		};
		return directive;
	};
  
  function BreadcrumbController() {
    var vm = this;
  };
  
})();

(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('itableComponent', itableComponent);

	function itableComponent() {
		var directive = {
        restrict: 'E',
				scope: {
					table: '='
				},
				templateUrl: 'app/directives/itable/itable.html',
        controller: TableController,
        controllerAs: 'vm',
        bindToController: true				
		};
		return directive;
	};
  
  function TableController() {
    var vm = this;
    
    vm.orderBy = function(column) {
      if (!column.orderable)
        return null;
      
      if (_.isUndefined(column.order))
        column.order = 'desc';
        
      if (column.order=='asc') {
        vm.table.data = _.sortBy(vm.table.data,column.id).reverse();
        column.order = 'desc';
      } else {
        vm.table.data = _.sortBy(vm.table.data,column.id)
        column.order = 'asc';
      }
      
      vm.table.orderedBy = _.indexOf(vm.table.columns,column);
    };
    
    if (!_.isUndefined(vm.table.orderBy)) 
      vm.orderBy(vm.table.columns[vm.table.orderBy]);
      
  };
  
})();

(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('loadingComponent', loadingComponent);

	function loadingComponent() {
		var directive = {
        restrict: 'E',
				templateUrl: 'app/directives/loading/loading.html',
		};
		return directive;
	};
  
})();

(function () {
	'use strict';

	angular
    .module('admin-ap')
    .directive('loginComponent', loginComponent);

	function loginComponent() {
		var directive = {
        restrict: 'E',
				scope: {
					onSubmit: '=',
          loading: '=?'
				},
				templateUrl: 'app/directives/login/login.html',
        controller: LoginController,
        controllerAs: 'vm',
        bindToController: true				
		};
		return directive;
	};
  
  function LoginController() {
    var vm = this;

    vm.login = {
      user: '',
      password: ''
    };
    
    if (_.isUndefined(vm.loading))
      vm.loading = false;
  };
  
})();

(function () {
  'use strict';
  
  DetailController.$inject = ['$log', '$stateParams', '$state', '$http', '$translate', '$scope', 'CONSTANTS'];
  angular
    .module('admin-ap')
    .controller('DetailController', DetailController);
  
  function DetailController($log, $stateParams, $state, $http, $translate, $scope, CONSTANTS) {
      $log.info("DetailController loaded.");
      
      var vm = this;
      
      vm.loading = true;
      
      vm.breadcrumb = [
        {active:false, label:'DETAIL.BREADCRUMB.PREV', link:'list'},
        {active:true, label:'DETAIL.BREADCRUMB.ME'}
      ];
      
      $http({
        method : "GET",
        url : CONSTANTS.USER_SERVICE
      }).then(function success(response) {        
        
        // The model object that we reference
        // on the  element in detail.html
        vm.item = {};
        
        // An array of our form fields with configuration
        // and options set. We make reference to this in
        // the 'fields' attribute on the  element
        vm.itemFields = [
            {
              key: 'name',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.NAME'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.NAME" | translate'
              }
            },
            {
              key: 'address',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.ADDRESS'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.ADDRESS" | translate'
              }
            },
            {
              key: 'state',
              type: 'input',
              templateOptions: {
                type: 'text',
                label: $translate.instant('DETAIL.FORM.STATE'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.STATE" | translate'
              }
            },
            {
              key: 'email',
              type: 'input',
              templateOptions: {
                type: 'email',
                label: $translate.instant('DETAIL.FORM.EMAIL'),
                required: true
              },
              expressionProperties: {
                'templateOptions.label': '"DETAIL.FORM.EMAIL" | translate'
              }
            },
        ];
        
        vm.item = response.data.data;        
        vm.loading = false;        
        
      }, function error(response) {
        vm.loading = false;
        $log.error(response.statusText);
      });
      
      vm.onSubmit = function() {
        console.log(vm.item);
      };
      
      $scope.$watch(function() {
        return $translate.use();
      }, function(newLang, oldLang) {
        if (newLang && oldLang && newLang !== oldLang) {
          angular.forEach(vm.itemFields, function(field) {
            field.runExpressions && field.runExpressions();
          });
        }
      });   
  };
    
})();

(function () {
  'use strict';
  
  ListController.$inject = ['$log', '$http', 'CONSTANTS'];
  angular
    .module('admin-ap')
    .controller('ListController', ListController);
  
  function ListController($log, $http, CONSTANTS) {
      $log.info("ListController loaded.");
      
      var vm = this;
      
      vm.loading = true;
      
      $http({
        method : "GET",
        url : CONSTANTS.USERS_SERVICE
      }).then(function success(response) {
        vm.table = {
          columns: [
            {id:'index', label:'LIST.DATA.ID', orderable:true},
            {id:'name', label:'LIST.DATA.NAME', orderable:false},
            {id:'email', label:'LIST.DATA.EMAIL', orderable:true}
          ],
          orderBy: 0, // optional
          id: 'index', // id field for details
          data: response.data.data
        };
        vm.loading = false;        
      }, function error(response) {
        vm.loading = false;
        $log.error(response.statusText);
      });
      
      
  };
    
})();

(function () {
  'use strict';
  
  LoginController.$inject = ['$log', '$state', 'authenticationService'];
  angular
    .module('admin-ap')
    .controller('LoginController', LoginController);
  
  function LoginController($log, $state, authenticationService) {
      $log.info("LoginController loaded.");
      
      var vm = this;
      
      vm.loading = false;
      
      vm.onSubmit = function(credentials) {
        vm.loading = true;
        // call auth service
        authenticationService.login(credentials.user, credentials.password)
          .then(function (data) {
            vm.loading = false;
            $state.go('list');
          })
          .catch(function (error) {
            vm.loading = true;
            vm.error = true;
            $log.error("AUTH ERROR > ");
            $log.error(error);
          });
      };
  };
    
})();
