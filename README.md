# Scratch

## Admin area starter project with:
- AngularJS (1.4.9)
- ui.router
- satellizer
- ng-translate
- formly
- and others util directives

## Get Started
- npm i
- bower i
- grunt